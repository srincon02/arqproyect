package in.bushansirgur.springrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArqProyectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArqProyectApplication.class, args);
	}
}
