package in.bushansirgur.springrestapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import in.bushansirgur.springrestapi.model.Employee;
@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

	List<Employee> findByName(String name);
	//SELECT * FROM table WHERE name= "sarita" AND location="Colombia"
	List<Employee> findByNameAndLocation(String name, String location);
	//SELECT * FROM table WHERE name LIKE "%ram%"
	List<Employee> findByNameContaining(String keyword, Sort sort);
	@Query("FROM Employee WHERE name = :name OR location = :location")
	List<Employee> getEmployeesByNameOrLocation(String name, String location);
	
	@Query("DELETE FROM Employee WHERE name = :name")
	Integer deleteEmployeeByName(String name);

	Employee save(Employee employee);

	Optional<Employee> findById(Long id);

	void deleteById(Long id);

}
